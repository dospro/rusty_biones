use super::{MapperInterface, MirrorType};

pub struct NROM {
    rom: Vec<u8>,
    sram: [u8; 0x2000],
    vrom: Vec<u8>,
    mirror_type: MirrorType,
    has_vram: bool,
}

impl NROM {
    pub fn new(mut rom: Vec<Vec<u8>>, mut vrom: Vec<Vec<u8>>, mirror_type: MirrorType) -> Self {
        let mut result_rom: Vec<u8>;
        if rom.len() < 2 {
            result_rom = rom[0].clone();
            result_rom.append(&mut rom[0]);
        } else {
            result_rom = std::mem::replace(&mut rom, Vec::new()).into_iter().flatten().collect();
        }
        let has_vram;
        let result_vrom = if vrom.is_empty() {
            has_vram = true;
            vec![0; 0x2000]
        } else {
            has_vram = false;
            std::mem::replace(&mut vrom, Vec::new()).into_iter().flatten().collect()
        };

        NROM {
            rom: result_rom,
            sram: [0; 0x2000],
            vrom: result_vrom,
            mirror_type,
            has_vram,
        }
    }
}

impl MapperInterface for NROM {
    fn cpu_read_request(&mut self, address: u16) -> u8 {
        if address < 0x4020 {
            panic!("MMC doesn't map addresses below 0x4020");
        } else if address < 0x6000 { // No expansion ROM
            return 0xFF;
        } else if address < 0x8000 {
            return self.sram[(address as usize) - 0x6000];
        } else {
            return self.rom[(address as usize) - 0x8000];
        };
    }

    fn cpu_write_request(&mut self, address: u16, value: u8) {
        match address {
            0x6000...0x7FFF => {
                self.sram[(address as usize) - 0x6000] = value;
            }
            _ => {}
        }
    }

    fn ppu_read_request(&mut self, address: u16) -> u8 {
        match address {
            0x0000...0x1FFF => self.vrom[address as usize],
            _ => { 0xFF }
        }
    }

    fn ppu_write_request(&mut self, address: u16, value: u8) {
        if self.has_vram {
            if address < 0x2000 {
                self.vrom[address as usize] = value;
            }
        }
    }

    fn get_mirror_type(&self) -> MirrorType {
        return self.mirror_type;
    }
}