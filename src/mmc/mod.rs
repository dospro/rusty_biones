pub mod nrom;

#[derive(Copy, Clone)]
pub enum MirrorType {
    SingleScreen,
    HorizontalMirror,
    VerticalMirror,
    FourScreen,
}

pub trait MapperInterface {
    fn cpu_read_request(&mut self, address: u16) -> u8;
    fn cpu_write_request(&mut self, address: u16, value: u8);
    fn ppu_read_request(&mut self, address: u16) -> u8;
    fn ppu_write_request(&mut self, address: u16, value: u8);

    fn get_mirror_type(&self) -> MirrorType;
}
