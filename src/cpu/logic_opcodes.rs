use super::Cpu;
use super::status_register::Flags;
use super::ScreenBuffer;

impl Cpu {
    pub(super) fn and(&mut self, value: u8) {
        self.a &= value;
        self.p.update(Flags::ZFlag, self.a == 0);
        self.p.update(Flags::NFlag, (self.a & 0x80) != 0);
    }

    pub(super) fn cmp(&mut self, register: u8, value: u8) {
        let result = register.wrapping_sub(value);
        self.p.update(Flags::CFlag, register >= value);
        self.p.update(Flags::ZFlag, result == 0);
        self.p.update(Flags::NFlag, (result & 0x80) != 0);
    }

    pub(super) fn xor(&mut self, value: u8) {
        self.a ^= value;
        self.p.update(Flags::ZFlag, self.a == 0);
        self.p.update(Flags::NFlag, (self.a & 0x80) != 0);
    }

    pub(super) fn or(&mut self, value: u8) {
        self.a |= value;
        self.p.update(Flags::ZFlag, self.a == 0);
        self.p.update(Flags::NFlag, (self.a & 0x80) != 0);
    }
}