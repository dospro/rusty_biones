use super::Cpu;
use super::status_register::{StatusRegister, Flags};
use super::ScreenBuffer;

impl Cpu {
    pub(super) fn asl(&mut self, address: u16) {
        let value = self.memory.read_byte(address);
        let result = (value as u16) << 1;
        self.p = StatusRegister { // Functional proof of concept
            carry_flag: (result & 0x100) != 0,
            zero_flag: result == 0,
            negative_flag: (result & 0x80) != 0,
            ..self.p
        };
        self.memory.write_byte(address, result as u8);
    }

    pub(super) fn bit(&mut self, value: u8) {
        let result = self.a & value;
        self.p.update(Flags::ZFlag, result == 0);
        self.p.update(Flags::VFlag, (result & 0x40) != 0);
        self.p.update(Flags::NFlag, (result & 0x80) != 0);
    }

    pub(super) fn lsr(&mut self, address: u16) {
        let value = self.memory.read_byte(address);
        self.p.update(Flags::CFlag, (value & 1) != 0);

        let result = value >> 1;
        self.p.update(Flags::ZFlag, result == 0);
        self.p.update(Flags::NFlag, false);
        self.memory.write_byte(address, result);
    }

    pub(super) fn rol(&mut self, address: u16) {
        let value = self.memory.read_byte(address);
        let result = ((value as u16) << 1) | self.p.get_flag_value(Flags::CFlag) as u16;
        self.p.update(Flags::CFlag, (result & 0x100) != 0);
        self.p.update(Flags::ZFlag, result == 0);
        self.p.update(Flags::NFlag, (result & 0x80) != 0);
        self.memory.write_byte(address, result as u8);
    }

    pub(super) fn ror(&mut self, address: u16) {
        let value = self.memory.read_byte(address);
        let carry_flag_value = self.p.get_flag_value(Flags::CFlag);
        self.p.update(Flags::CFlag, (value & 1) != 0);

        let result = (value >> 1) | (carry_flag_value << 7);
        self.p.update(Flags::ZFlag, result == 0);
        self.p.update(Flags::NFlag, (result & 0x80) != 0);
        self.memory.write_byte(address, (result & 0xFF) as u8);
    }
}