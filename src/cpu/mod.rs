mod status_register;
mod arithmetic_opcodes;
mod bit_opcodes;
mod jump_opcodes;
mod logic_opcodes;
mod registers_opcodes;

use std::collections::HashSet;
use super::memory_map::{MemoryMap, InterruptType};
use super::ppu::ScreenBuffer;
use super::control_pad::NESButtons;
use self::status_register::{StatusRegister, Flags};

const CYCLES_PER_FRAME: u32 = 262 * 341 / 3;

const OPCODE_CYCLES: [u8; 0x100] = [4; 0x100];

fn get_cycles() -> [u8; 0x100] {
    let mut cycles: [u8; 0x100] = [0; 0x100];
    // ADC              AND                 ASL                 BCS
    cycles[0x69] = 2;   cycles[0x29] = 2;   cycles[0x0A] = 2;   cycles[0xB0] = 2;
    cycles[0x65] = 3;   cycles[0x25] = 3;   cycles[0x06] = 5;   /*BEQ*/
    cycles[0x75] = 4;   cycles[0x35] = 4;   cycles[0x16] = 6;   cycles[0xF0] = 2;
    cycles[0x6D] = 4;   cycles[0x2D] = 4;   cycles[0x0E] = 6;   /*BIT*/
    cycles[0x7D] = 4;   cycles[0x3D] = 4;   cycles[0x1E] = 7;   cycles[0x24] = 3;
    cycles[0x79] = 4;   cycles[0x39] = 4;   /*BCC*/             cycles[0x2C] = 4;
    cycles[0x61] = 6;   cycles[0x21] = 6;   cycles[0x90] = 2;   /*BMI*/
    cycles[0x71] = 5;   cycles[0x31] = 5;                       cycles[0x30] = 2;

    // BNE              BVS                 CLV
    cycles[0xD0] = 2;   cycles[0x70] = 2;   cycles[0xB8] = 2;   cycles[0xD9] = 4;
    /*BPL*/             /*CLC*/             /*CMP*/             cycles[0xC1] = 6;
    cycles[0x10] = 2;   cycles[0x18] = 2;   cycles[0xC9] = 2;   cycles[0xD1] = 5;
    /*BRK*/             /*CLD*/             cycles[0xC5] = 3;   /*CPX*/
    cycles[0x00] = 7;   cycles[0xD8] = 2;   cycles[0xD5] = 4;   cycles[0xE0] = 2;
    /*BVC*/             /*CLI*/             cycles[0xCD] = 4;   cycles[0xE4] = 3;
    cycles[0x50] = 2;   cycles[0x58] = 2;   cycles[0xDD] = 4;   cycles[0xEC] = 4;

    // CPY              DEX
    cycles[0xC0] = 2;   cycles[0xCA] = 2;   cycles[0x5D] = 4;   cycles[0xFE] = 7;
    cycles[0xC4] = 3;   /*DEY*/             cycles[0x59] = 4;   /*INCX*/
    cycles[0xCC] = 4;   cycles[0x88] = 2;   cycles[0x41] = 6;   cycles[0xE8] = 2;
    /*DEC*/             /*EOR*/             cycles[0x51] = 5;   /*INY*/
    cycles[0xC6] = 5;   cycles[0x49] = 2;   /*INC*/             cycles[0xC8] = 2;
    cycles[0xD6] = 6;   cycles[0x45] = 3;   cycles[0xE6] = 5;   /*JMP*/
    cycles[0xCE] = 6;   cycles[0x55] = 4;   cycles[0xF6] = 6;   cycles[0x4C] = 3;
    cycles[0xDE] = 7;   cycles[0x4D] = 4;   cycles[0xEE] = 6;   cycles[0x6C] = 5;

    // JSR              LDX                                     ORA
    cycles[0x20] = 6;   cycles[0xA2] = 2;   cycles[0xBC] = 4;   cycles[0x09] = 2;
    /*LDA*/             cycles[0xA6] = 3;   /*LSR*/             cycles[0x05] = 3;
    cycles[0xA9] = 2;   cycles[0xB6] = 4;   cycles[0x4A] = 2;   cycles[0x15] = 4;
    cycles[0xA5] = 3;   cycles[0xAE] = 4;   cycles[0x46] = 5;   cycles[0x0D] = 4;
    cycles[0xB5] = 4;   cycles[0xBE] = 4;   cycles[0x56] = 6;   cycles[0x1D] = 4;
    cycles[0xAD] = 4;   /*LDY*/             cycles[0x4E] = 6;   cycles[0x19] = 4;
    cycles[0xBD] = 4;   cycles[0xA0] = 2;   cycles[0x5E] = 7;   cycles[0x01] = 6;
    cycles[0xB9] = 4;   cycles[0xA4] = 3;   /*NOP*/             cycles[0x11] = 5;
    cycles[0xA1] = 6;   cycles[0xB4] = 4;   cycles[0xEA] = 2;   /*PHA*/
    cycles[0xB1] = 5;   cycles[0xAC] = 4;                       cycles[0x48] = 3;

    // PHP
    cycles[0x08] = 3;   cycles[0x36] = 6;   cycles[0x7E] = 7;   cycles[0xF5] = 4;
    /*PLA*/             cycles[0x2E] = 6;   /*RTI*/             cycles[0xED] = 4;
    cycles[0x68] = 4;   cycles[0x3E] = 7;   cycles[0x40] = 6;   cycles[0xFD] = 4;
    /*PLP*/             /*ROR*/             /*RTS*/             cycles[0xF9] = 4;
    cycles[0x28] = 4;   cycles[0x6A] = 2;   cycles[0x60] = 6;   cycles[0xE1] = 6;
    /*ROL*/             cycles[0x66] = 5;   /*SBC*/             cycles[0xF1] = 5;
    cycles[0x2A] = 2;   cycles[0x76] = 6;   cycles[0xE9] = 2;   /*SEC*/
    cycles[0x26] = 5;   cycles[0x6E] = 6;   cycles[0xE5] = 3;   cycles[0x38] = 2;

    // SED                                                      TXS
    cycles[0xF8] = 2;   cycles[0x81] = 6;   cycles[0x8C] = 4;   cycles[0x9A] = 2;
    /*SEI*/             cycles[0x91] = 6;   /*TAX*/             /*TYA*/
    cycles[0x78] = 2;   /*STX*/             cycles[0xAA] = 2;   cycles[0x98] = 2;
    /*STA*/             cycles[0x86] = 3;   /*TAY*/
    cycles[0x85] = 3;   cycles[0x96] = 4;   cycles[0xA8] = 2;
    cycles[0x95] = 4;   cycles[0x8E] = 4;   /*TSX*/
    cycles[0x5D] = 4;   /*STY*/             cycles[0xBA] = 2;
    cycles[0x9D] = 5;   cycles[0x84] = 3;   /*TXA*/
    cycles[0x99] = 5;   cycles[0x94] = 4;   cycles[0x8A] = 2;
    return cycles;
}


pub struct Cpu {
    pc: u16,
    sp: u8,
    a: u8,
    p: StatusRegister,
    x: u8,
    y: u8,
    memory: MemoryMap,
    running: bool,
    cycles: [u8; 0x100],
    cycles_counter: u32,
}

// public
impl Cpu {
    pub fn new(filename: &str) -> Cpu {
        let mut memory = MemoryMap::new(filename).unwrap();
        let pc = memory.read_word(0xFFFC);
        let p = StatusRegister::from(0x34);
        Cpu {
            pc,
            sp: 0,
            a: 0,
            p,
            x: 0,
            y: 0,
            memory,
            running: true,
            cycles: get_cycles(),
            cycles_counter: 0,
        }
    }

    pub fn run_frame(&mut self) {
        while self.cycles_counter < CYCLES_PER_FRAME {
            let c = self.execute_opcode();
            self.handle_interrupts();
            self.memory.update(c as u32);
            self.cycles_counter += c as u32;
        }
        self.cycles_counter -= CYCLES_PER_FRAME;
    }

    pub fn get_framebuffer(&self) -> &ScreenBuffer {
        return self.memory.get_framebuffer();
    }

    pub fn update_input(&mut self, input: HashSet<NESButtons>) {
        self.memory.update_input(input);
    }
}

// private
impl Cpu {
    fn immediate_read(&mut self) -> u8 {
        let address = self.pc;
        self.pc += 1;
        self.memory.read_byte(address)
    }

    fn zero_page_read(&mut self) -> u8 {
        let address = self.memory.read_byte(self.pc) as u16;
        self.pc += 1;
        self.memory.read_byte(address)
    }

    fn indexed_zero_page_read(&mut self, index: u8) -> u8 {
        let address = ((self.memory.read_byte(self.pc) + index) & 0xFF) as u16;
        self.pc += 1;
        self.memory.read_byte(address)
    }

    fn absolute_read(&mut self) -> u8 {
        let address = self.memory.read_word(self.pc);
        self.pc += 2;
        self.memory.read_byte(address)
    }

    fn indexed_absolute_read(&mut self, index: u8) -> u8 {
        let address = self.memory.read_word(self.pc) + index as u16;
        self.pc += 2;
        self.memory.read_byte(address)
    }

    fn indexed_indirect_read(&mut self, index: u8) -> u8 {
        let address = (self.memory.read_byte(self.pc) as u16) + index as u16;
        let address = self.memory.read_word(address);
        self.pc += 1;
        self.memory.read_byte(address)
    }

    fn indirect_indexed_read(&mut self, index: u8) -> u8 {
        let address = self.memory.read_byte(self.pc) as u16;
        let address = self.memory.read_word(address) + index as u16;
        self.pc += 1;
        self.memory.read_byte(address)
    }

    fn push(&mut self, value: u8) {
        let address = 0x100 | self.sp as u16;
        self.memory.write_byte(address, value);
        self.sp = self.sp.wrapping_sub(1);
    }

    fn pop(&mut self) -> u8 {
        self.sp = self.sp.wrapping_add(1);
        let address = 0x100 | self.sp as u16;
        self.memory.read_byte(address)
    }
}

impl Cpu {
    fn handle_interrupts(&mut self) {
        let interrupt = self.memory.get_next_interrupt();
        match interrupt {
            InterruptType::NMI => {
                self.push((self.pc >> 8) as u8);
                self.push((self.pc & 0xFF) as u8);
                self.push(u8::from(&self.p));
                let address = self.memory.read_word(0xFFFA);
                self.pc = address;
                self.p.update(Flags::IFlag, true);
            }
            _ => {}
        }
    }
    pub fn execute_opcode(&mut self) -> u8 {
        let opcode = self.immediate_read();

        match opcode {
            // ADC
            0x69 => {
                let value = self.immediate_read();
                self.adc(value);
            }
            0x65 => {
                let value = self.zero_page_read();
                self.adc(value);
            }
            0x75 => {
                let value = self.indexed_zero_page_read(self.x);
                self.adc(value)
            }
            0x6d => {
                let value = self.absolute_read();
                self.adc(value);
            }
            0x7d => {
                let value = self.indexed_absolute_read(self.x);
                self.adc(value);
            }
            0x79 => {
                let value = self.indexed_absolute_read(self.y);
                self.adc(value);
            }
            0x61 => {
                let value = self.indexed_indirect_read(self.x);
                self.adc(value);
            }
            0x71 => {
                let value = self.indirect_indexed_read(self.y);
                self.adc(value);
            }

            // AND
            0x29 => {
                let value = self.immediate_read();
                self.and(value);
            }
            0x25 => {
                let value = self.zero_page_read();
                self.and(value);
            }
            0x35 => {
                let value = self.indexed_zero_page_read(self.x);
                self.and(value);
            }
            0x2d => {
                let value = self.absolute_read();
                self.and(value);
            }
            0x3d => {
                let value = self.indexed_absolute_read(self.x);
                self.and(value);
            }
            0x39 => {
                let value = self.indexed_absolute_read(self.y);
                self.and(value);
            }
            0x21 => {
                let value = self.indexed_indirect_read(self.x);
                self.and(value);
            }
            0x31 => {
                let value = self.indirect_indexed_read(self.y);
                self.and(value);
            }

            // ASL - Arithmetic Shift Left
            0x0a => {
                let result: u16 = (self.a << 1) as u16;
                self.p.update(Flags::CFlag, (result & 0x100) != 0);
                self.p.update(Flags::ZFlag, result == 0);
                self.p.update(Flags::NFlag, (result & 0x80) != 0);
                self.a = (result & 0xFF) as u8;
            }
            0x06 => {
                let address = self.immediate_read() as u16;
                self.asl(address);
            }
            0x16 => {
                let address = ((self.immediate_read() + self.x) & 0xFF) as u16;
                self.asl(address)
            }
            0x0e => {
                let address = self.memory.read_word(self.pc);
                self.asl(address);
                self.pc += 2;
            }
            0x1e => {
                let address = self.memory.read_word(self.pc);
                self.asl(address + self.x as u16);
                self.pc += 2;
            }

            0x90 => { // BCC - Branch if carry clear
                let value = self.immediate_read();
                self.bcc(value);
            }
            0xb0 => { // BCS - Branch if carry is set
                let value = self.immediate_read();
                self.bcs(value);
            }
            0xf0 => { // BEQ - Branch if equal
                let value = self.immediate_read();
                self.beq(value);
            }

            // BIT - Bit Test
            0x24 => {
                let value = self.zero_page_read();
                self.bit(value);
            }
            0x2c => {
                let value = self.absolute_read();
                self.bit(value);
            }

            0x30 => { // BMI - Branch if minus
                let value = self.immediate_read();
                self.bmi(value);
            }
            0xd0 => { // BNE - Branch if not Equal
                let value = self.immediate_read();
                self.bne(value);
            }
            0x10 => { // BPL - Branch if positive
                let value = self.immediate_read();
                self.bpl(value);
            }
            0x00 => self.brk(), // BRK - Force Interrupt
            0x50 => { // BVC - Branch if overflow clear
                let value = self.immediate_read();
                self.bvc(value);
            }
            0x70 => { // BVS - Branch if overflow set
                let value = self.immediate_read();
                self.bvs(value);
            }

            0x18 => self.p.update(Flags::CFlag, false), // CLC - Clear Carry Flag
            0xd8 => self.p.update(Flags::DFlag, false), // CLD - Clear Decimal mode
            0x58 => self.p.update(Flags::IFlag, false), // CLI - Clear Interrupt disable
            0xb8 => self.p.update(Flags::VFlag, false), // CLV - Clear Overflow Flag

            // CMP - Compare
            0xc9 => {
                let value = self.immediate_read();
                self.cmp(self.a, value);
            }
            0xc5 => {
                let value = self.zero_page_read();
                self.cmp(self.a, value);
            }
            0xd5 => {
                let value = self.indexed_zero_page_read(self.x);
                self.cmp(self.a, value);
            }
            0xcd => {
                let value = self.absolute_read();
                self.cmp(self.a, value);
            }
            0xdd => {
                let value = self.indexed_absolute_read(self.x);
                self.cmp(self.a, value);
            }
            0xd9 => {
                let value = self.indexed_absolute_read(self.y);
                self.cmp(self.a, value);
            }
            0xc1 => {
                let value = self.indexed_indirect_read(self.x);
                self.cmp(self.a, value);
            }
            0xd1 => {
                let value = self.indirect_indexed_read(self.y);
                self.cmp(self.a, value);
            }

            // CPX - Compare X Register
            0xe0 => {
                let value = self.immediate_read();
                self.cmp(self.x, value);
            }
            0xe4 => {
                let value = self.zero_page_read();
                self.cmp(self.x, value);
            }
            0xec => {
                let value = self.absolute_read();
                self.cmp(self.x, value);
            }

            // CPY - Compare Y Register
            0xc0 => {
                let value = self.immediate_read();
                self.cmp(self.y, value);
            }
            0xc4 => {
                let value = self.zero_page_read();
                self.cmp(self.y, value);
            }
            0xcc => {
                let value = self.absolute_read();
                self.cmp(self.y, value);
            }

            // DEC . Decrement Memory
            0xc6 => {
                let address = self.immediate_read() as u16;
                self.dec(address);
            }
            0xd6 => {
                let address = (self.immediate_read() + self.x) as u16;
                self.dec(address);
            }
            0xce => {
                let address = self.memory.read_word(self.pc);
                self.pc += 2;
                self.dec(address);
            }
            0xde => {
                let address = self.memory.read_word(self.pc) + self.x as u16;
                self.pc += 2;
                self.dec(address);
            }

            //DEX - Decrement X Register
            0xca => self.dex(),
            0x88 => self.dey(),

            // EOR (XOR) - Exclusive OR
            0x49 => {
                let value = self.immediate_read();
                self.xor(value);
            }
            0x45 => {
                let value = self.zero_page_read();
                self.xor(value);
            }
            0x55 => {
                let value = self.indexed_zero_page_read(self.x);
                self.xor(value);
            }
            0x4d => {
                let value = self.absolute_read();
                self.xor(value);
            }
            0x5d => {
                let value = self.indexed_absolute_read(self.x);
                self.xor(value);
            }
            0x59 => {
                let value = self.indexed_absolute_read(self.y);
                self.xor(value);
            }
            0x41 => {
                let value = self.indexed_indirect_read(self.x);
                self.xor(value);
            }
            0x51 => {
                let value = self.indirect_indexed_read(self.y);
                self.xor(value);
            }

            // INC - Increment memory
            0xe6 => {
                let address = self.immediate_read() as u16;
                self.inc(address);
            }
            0xf6 => {
                let address = (self.immediate_read() + self.x) as u16;
                self.inc(address);
            }
            0xee => {
                let address = self.memory.read_word(self.pc);
                self.pc += 2;
                self.inc(address);
            }
            0xfe => {
                let address = self.memory.read_word(self.pc) + self.x as u16;
                self.pc += 2;
                self.inc(address);
            }

            // INX - Increment X Register
            0xe8 => self.inx(),
            0xc8 => self.iny(),

            // JMP - Jump
            0x4c => self.jmp(self.pc),
            0x6c => {
                let address = self.memory.read_word(self.pc);
                self.jmp(address);
            }

            // JSR (CALL) - Jump to subroutine
            0x20 => {
                let address = self.memory.read_word(self.pc);
                self.pc += 2;
                self.jsr(address);
            }

            // LDA - Load A register
            0xa9 => {
                let value = self.immediate_read();
                self.lda(value);
            }
            0xa5 => {
                let value = self.zero_page_read();
                self.lda(value);
            }
            0xb5 => {
                let value = self.indexed_zero_page_read(self.x);
                self.lda(value);
            }
            0xad => {
                let value = self.absolute_read();
                self.lda(value);
            }
            0xbd => {
                let value = self.indexed_absolute_read(self.x);
                self.lda(value);
            }
            0xb9 => {
                let value = self.indexed_absolute_read(self.y);
                self.lda(value);
            }
            0xa1 => {
                let value = self.indexed_indirect_read(self.x);
                self.lda(value);
            }
            0xb1 => {
                let value = self.indirect_indexed_read(self.y);
                self.lda(value);
            }

            // LDX - Load X Register
            0xa2 => {
                let value = self.immediate_read();
                self.ldx(value);
            }
            0xa6 => {
                let value = self.zero_page_read();
                self.ldx(value);
            }
            0xb6 => {
                let value = self.indexed_zero_page_read(self.y);
                self.ldx(value);
            }
            0xae => {
                let value = self.absolute_read();
                self.ldx(value);
            }
            0xbe => {
                let value = self.indexed_absolute_read(self.y);
                self.ldx(value);
            }

            // LDY - Load Y Register
            0xa0 => {
                let value = self.immediate_read();
                self.ldy(value);
            }
            0xa4 => {
                let value = self.zero_page_read();
                self.ldy(value);
            }
            0xb4 => {
                let value = self.indexed_zero_page_read(self.x);
                self.ldy(value);
            }
            0xac => {
                let value = self.absolute_read();
                self.ldy(value);
            }
            0xbc => {
                let value = self.indexed_absolute_read(self.x);
                self.ldy(value);
            }

            // LSR - Logical Shift Right
            0x4a => {
                self.p.update(Flags::CFlag, (self.a & 1) != 0);
                self.a >>= 1;
                self.p.update(Flags::ZFlag, self.a == 0);
                self.p.update(Flags::NFlag, false);
            }
            0x46 => {
                let address = self.immediate_read() as u16;
                self.lsr(address);
            }
            0x56 => {
                let address = (self.immediate_read() + self.x) as u16;
                self.lsr(address);
            }
            0x4e => {
                let address = self.memory.read_word(self.pc);
                self.lsr(address);
                self.pc += 2;
            }
            0x5e => {
                let address = self.memory.read_word(self.pc) + self.x as u16;
                self.lsr(address);
                self.pc += 2;
            }

            // NOP
            0xea => (),

            0x09 => {
                let value = self.immediate_read();
                self.or(value);
            }
            0x05 => {
                let value = self.zero_page_read();
                self.or(value);
            }
            0x15 => {
                let value = self.indexed_zero_page_read(self.x);
                self.or(value);
            }
            0x0d => {
                let value = self.absolute_read();
                self.or(value);
            }
            0x1d => {
                let value = self.indexed_absolute_read(self.x);
                self.or(value);
            }
            0x19 => {
                let value = self.indexed_absolute_read(self.y);
                self.or(value);
            }
            0x01 => {
                let value = self.indexed_indirect_read(self.x);
                self.or(value);
            }
            0x11 => {
                let value = self.indirect_indexed_read(self.y);
                self.or(value);
            }

            0x48 => self.push(self.a), // PHA - Push accumulator
            0x08 => self.push(u8::from(&self.p) | 0b11_0000), // PHP - Push processor status
            0x68 => { // PLA - Pull accumulator
                self.a = self.pop();
                self.p.update(Flags::ZFlag, self.a == 0);
                self.p.update(Flags::NFlag, (self.a & 0x80) != 0);
            }
            0x28 => { // PLP - Pull processor status
                let value = self.pop();
                self.p = StatusRegister::from(value);
            }

            // ROL - Rotate Left
            0x2a => {
                let result = ((self.a as u16) << 1) | self.p.get_flag_value(Flags::CFlag) as u16;
                self.p.update(Flags::CFlag, (result & 0x100) != 0);
                self.p.update(Flags::ZFlag, result == 0);
                self.p.update(Flags::NFlag, (result & 0x80) != 0);
                self.a = result as u8;
            }
            0x26 => {
                let address = self.immediate_read() as u16;
                self.rol(address);
            }
            0x36 => {
                let address = (self.immediate_read() as u16) + self.x as u16;
                self.rol(address);
            }
            0x2e => {
                let address = self.memory.read_word(self.pc);
                self.rol(address);
                self.pc += 2;
            }
            0x3e => {
                let address = self.memory.read_word(self.pc) + self.x as u16;
                self.rol(address);
                self.pc += 2;
            }

            // ROR - Rotate Right
            0x6a => {
                let carry_flag_value = self.p.get_flag_value(Flags::CFlag);
                self.p.update(Flags::CFlag, (self.a & 1) != 0);

                let result = (self.a >> 1) | (carry_flag_value << 7);
                self.p.update(Flags::ZFlag, result == 0);
                self.p.update(Flags::NFlag, (result & 0x80) != 0);
                self.a = result;
            }
            0x66 => {
                let address = self.immediate_read() as u16;
                self.ror(address);
            }
            0x76 => {
                let address = (self.immediate_read() as u16) + self.x as u16;
                self.ror(address);
            }
            0x6e => {
                let address = self.memory.read_word(self.pc);
                self.ror(address);
                self.pc += 2;
            }
            0x7e => {
                let address = self.memory.read_word(self.pc) + self.x as u16;
                self.ror(address);
                self.pc += 2;
            }

            // RTI - Return from interrupt
            0x40 => {
                let value = self.pop();
                self.p = StatusRegister::from(value);
                let low = self.pop() as u16;
                let high = self.pop() as u16;
                self.pc = (high << 8) | low;
            }

            // RTS - Return from subroutine
            0x60 => {
                let low = self.pop() as u16;
                let high = self.pop() as u16;
                self.pc = (high << 8) | low;
                self.pc += 1;
            }

            // SBC - Subtract with carry
            0xe9 => {
                let value = self.immediate_read();
                self.sbc(value);
            }
            0xe5 => {
                let value = self.zero_page_read();
                self.sbc(value);
            }
            0xf5 => {
                let value = self.indexed_zero_page_read(self.x);
                self.sbc(value)
            }
            0xed => {
                let value = self.absolute_read();
                self.sbc(value);
            }
            0xfd => {
                let value = self.indexed_absolute_read(self.x);
                self.sbc(value);
            }
            0xf9 => {
                let value = self.indexed_absolute_read(self.y);
                self.sbc(value);
            }
            0xe1 => {
                let value = self.indexed_indirect_read(self.x);
                self.sbc(value);
            }
            0xf1 => {
                let value = self.indirect_indexed_read(self.y);
                self.sbc(value);
            }

            0x38 => self.p.update(Flags::CFlag, true), // SEC - Set Carry Flag
            0xf8 => self.p.update(Flags::DFlag, true), // SED - Set Decimal Flag
            0x78 => self.p.update(Flags::IFlag, true), // SEC - Set Interrupt Disable

            // STA - Store Accumulator
            0x85 => {
                let address = self.immediate_read() as u16;
                self.memory.write_byte(address, self.a);
            }
            0x95 => {
                let address = (self.immediate_read() as u16) + self.x as u16;
                self.memory.write_byte(address, self.a);
            }
            0x8d => {
                let address = self.memory.read_word(self.pc);
                self.pc += 2;
                self.memory.write_byte(address, self.a);
            }
            0x9d => {
                let address = self.memory.read_word(self.pc) + self.x as u16;
                self.pc += 2;
                self.memory.write_byte(address, self.a);
            }
            0x99 => {
                let address = self.memory.read_word(self.pc) + self.y as u16;
                self.pc += 2;
                self.memory.write_byte(address, self.a);
            }
            0x81 => {
                let address = (self.immediate_read() as u16) + self.x as u16;
                let address = self.memory.read_word(address);
                self.memory.write_byte(address, self.a);
            }
            0x91 => {
                let address = self.immediate_read() as u16;
                let address = self.memory.read_word(address) + self.y as u16;
                self.memory.write_byte(address, self.a);
            }

            // STX - Store X Register
            0x86 => {
                let address = self.immediate_read() as u16;
                self.memory.write_byte(address, self.x);
            }
            0x96 => {
                let address = (self.immediate_read() as u16) + self.y as u16;
                self.memory.write_byte(address, self.x);
            }
            0x8e => {
                let address = self.memory.read_word(self.pc);
                self.pc += 2;
                self.memory.write_byte(address, self.x);
            }

            // STY - Store Y Register
            0x84 => {
                let address = self.immediate_read() as u16;
                self.memory.write_byte(address, self.y);
            }
            0x94 => {
                let address = (self.immediate_read() as u16) + self.x as u16;
                self.memory.write_byte(address, self.y);
            }
            0x8c => {
                let address = self.memory.read_word(self.pc);
                self.pc += 2;
                self.memory.write_byte(address, self.y);
            }

            0xaa => { // TAX - Transfer accumulator to X
                self.x = self.a;
                self.p.update(Flags::ZFlag, self.x == 0);
                self.p.update(Flags::NFlag, (self.x & 0x80) != 0);
            }
            0xa8 => { // TAY - Transfer accumulator to Y
                self.y = self.a;
                self.p.update(Flags::ZFlag, self.y == 0);
                self.p.update(Flags::NFlag, (self.y & 0x80) != 0);
            }
            0xba => { // TSX - Transfer SP to X
                self.x = self.sp;
                self.p.update(Flags::ZFlag, self.x == 0);
                self.p.update(Flags::NFlag, (self.x & 0x80) != 0);
            }
            0x8a => { // TXA - Transfer X to accumulator
                self.a = self.x;
                self.p.update(Flags::ZFlag, self.a == 0);
                self.p.update(Flags::NFlag, (self.a & 0x80) != 0);
            }
            0x9a => { // TXS - Transfer X to sp
                self.sp = self.x;
                self.p.update(Flags::ZFlag, self.sp == 0);
                self.p.update(Flags::NFlag, (self.sp & 0x80) != 0);
            }
            0x98 => { // TYA - Transfer Y to accumulator
                self.a = self.y;
                self.p.update(Flags::ZFlag, self.a == 0);
                self.p.update(Flags::NFlag, (self.a & 0x80) != 0);
            }

            _ => println!("Unkown opcode: {} at pc {}", opcode, self.pc)
        };
        return self.cycles[opcode as usize];
    }
}