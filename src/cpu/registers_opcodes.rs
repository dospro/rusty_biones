use super::Cpu;
use super::status_register::StatusRegister;
use super::ScreenBuffer;

impl Cpu {
    pub(super) fn lda(&mut self, value: u8) {
        self.a = value;
        self.p = StatusRegister {
            zero_flag: self.a == 0,
            negative_flag: (self.a & 0x80) != 0,
            ..self.p
        };
    }

    pub(super) fn ldx(&mut self, value: u8) {
        self.x = value;
        self.p.zero_flag = self.x == 0;
        self.p.negative_flag = (self.x & 0x80) != 0;
    }

    pub(super) fn ldy(&mut self, value: u8) {
        self.y = value;
        self.p.zero_flag = self.y == 0;
        self.p.negative_flag = (self.y & 0x80) != 0;
    }
}