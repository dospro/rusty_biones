use super::Cpu;
use super::status_register::Flags;
use super::ScreenBuffer;

impl Cpu {
    pub(super) fn bcc(&mut self, value: u8) {
        if self.p.get_flag_value(Flags::CFlag) == 0 {
            self.pc = (self.pc as i32 + (value as i8) as i32) as u16;
        }
    }

    pub(super) fn bcs(&mut self, value: u8) {
        if self.p.get_flag_value(Flags::CFlag) == 1 {
            self.pc = (self.pc as i32 + (value as i8) as i32) as u16;
        }
    }

    pub(super) fn beq(&mut self, value: u8) {
        if self.p.get_flag_value(Flags::ZFlag) == 1 {
            self.pc = (self.pc as i32 + (value as i8) as i32) as u16;
        }
    }

    pub(super) fn bmi(&mut self, value: u8) {
        if self.p.get_flag_value(Flags::NFlag) == 1 {
            self.pc = (self.pc as i32 + (value as i8) as i32) as u16;
        }
    }

    pub(super) fn bne(&mut self, value: u8) {
        if self.p.get_flag_value(Flags::ZFlag) == 0 {
            self.pc = (self.pc as i32 + (value as i8) as i32) as u16;
        }
    }

    pub(super) fn bpl(&mut self, value: u8) {
        if self.p.get_flag_value(Flags::NFlag) == 0 {
            self.pc = (self.pc as i32 + (value as i8) as i32) as u16;
        }
    }

    pub(super) fn brk(&mut self) {
        self.pc += 1;
        self.push((self.pc >> 8) as u8);
        self.push((self.pc & 0xFF) as u8);
        self.push(u8::from(&self.p) | 0b11_0000);
        self.p.interrupt_disable = true;
        self.pc = self.memory.read_word(0xFFFE);
    }

    pub(super) fn bvc(&mut self, value: u8) {
        if self.p.get_flag_value(Flags::VFlag) == 0 {
            self.pc = (self.pc as i32 + (value as i8) as i32) as u16;
        }
    }

    pub(super) fn bvs(&mut self, value: u8) {
        if self.p.get_flag_value(Flags::VFlag) == 1 {
            self.pc = (self.pc as i32 + (value as i8) as i32) as u16;
        }
    }

    pub(super) fn jmp(&mut self, address: u16) {
        self.pc = self.memory.read_word(address);
    }

    pub(super) fn jsr(&mut self, address: u16) {
        self.pc -= 1;
        self.push((self.pc >> 8) as u8);
        self.push((self.pc & 0xFF) as u8);
        self.pc = address;
    }
}