use super::Cpu;
use super::status_register::Flags;
use super::ScreenBuffer;

impl Cpu{
    pub(super) fn adc(&mut self, value: u8) {
        let carry_flag_value = self.p.get_flag_value(Flags::CFlag);
        let result = (self.a as u16) + (value as u16) + carry_flag_value as u16;
        self.p.update(Flags::CFlag, result > 0xFF);
        self.p.update(Flags::ZFlag, (result & 0xFF) == 0);
        self.p.update(Flags::VFlag, (!(self.a ^ value) & (self.a ^ result as u8) & 0x80) != 0);
        self.p.update(Flags::NFlag, (result & 0x80) != 0);
        self.a = result as u8;
        self.cycles_counter += 1;
    }

    pub(super) fn dec(&mut self, address: u16) {
        let value = self.memory.read_byte(address).wrapping_sub(1);
        self.p.update(Flags::ZFlag, value == 0);
        self.p.update(Flags::NFlag, (value & 0x80) != 0);
        self.memory.write_byte(address, value);
    }

    pub(super) fn dex(&mut self) {
        self.x = self.x.wrapping_sub(1);
        self.p.update(Flags::ZFlag, self.x == 0);
        self.p.update(Flags::NFlag, (self.x & 0x80) != 0);
    }

    pub(super) fn dey(&mut self) {
        self.y = self.y.wrapping_sub(1);
        self.p.update(Flags::ZFlag, self.y == 0);
        self.p.update(Flags::NFlag, (self.y & 0x80) != 0);
    }
    pub(super) fn inc(&mut self, address: u16) {
        let value = self.memory.read_byte(address).wrapping_add(1);
        self.p.update(Flags::ZFlag, value == 0);
        self.p.update(Flags::NFlag, (value & 0x80) != 0);
        self.memory.write_byte(address, value);
    }

    pub(super) fn inx(&mut self) {
        self.x = self.x.wrapping_add(1);
        self.p.update(Flags::ZFlag, self.x == 0);
        self.p.update(Flags::NFlag, (self.x & 0x80) != 0);
    }

    pub(super) fn iny(&mut self) {
        self.y = self.y.wrapping_add(1);
        self.p.update(Flags::ZFlag, self.y == 0);
        self.p.update(Flags::NFlag, (self.y & 0x80) != 0);
    }

    pub(super) fn sbc(&mut self, value: u8) {
        let carry_flag_value = self.p.get_flag_value(Flags::CFlag);
        let result = ((self.a as i16) - (value as i16) - (carry_flag_value ^ 1) as i16) as u8;
        self.p.update(Flags::CFlag, self.a <= result);
        self.p.update(Flags::ZFlag, (result & 0xFF) == 0);
        self.p.update(Flags::VFlag, (!(self.a ^ value) & (self.a ^ result as u8) & 0x80) != 0);
        self.p.update(Flags::NFlag, (result & 0x80) != 0);
        self.a = result;
    }
}
