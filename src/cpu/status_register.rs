pub enum Flags {
    CFlag,
    ZFlag,
    IFlag,
    DFlag,
    VFlag,
    NFlag,
}

pub struct StatusRegister {
    pub carry_flag: bool,
    pub zero_flag: bool,
    pub interrupt_disable: bool,
    pub decimal_mode_flag: bool,
    pub overflow_flag: bool,
    pub negative_flag: bool,
}

impl From<u8> for StatusRegister {
    fn from(value: u8) -> Self {
        StatusRegister {
            negative_flag: (value & 0x80) != 0,
            overflow_flag: (value & 0x40) != 0,
            decimal_mode_flag: (value & 0x8) != 0,
            interrupt_disable: (value & 0x4) != 0,
            zero_flag: (value & 0x2) != 0,
            carry_flag: (value & 0x1) != 0,
        }
    }
}

impl From<&StatusRegister> for u8 {
    fn from(value: &StatusRegister) -> Self {
        ((value.negative_flag as u8) << 7) |
            ((value.overflow_flag as u8) << 6) |
            0b0010_0000 |
            ((value.decimal_mode_flag as u8) << 3) |
            ((value.interrupt_disable as u8) << 2) |
            ((value.zero_flag as u8) << 1) |
            (value.carry_flag as u8)
    }
}

impl StatusRegister {
    pub fn new() -> StatusRegister {
        StatusRegister {
            carry_flag: false,
            zero_flag: false,
            interrupt_disable: false,
            decimal_mode_flag: false,
            overflow_flag: false,
            negative_flag: false,
        }
    }

    pub fn update(&mut self, flag: Flags, value: bool) {
        match flag {
            Flags::CFlag => { self.carry_flag = value; }
            Flags::ZFlag => { self.zero_flag = value; }
            Flags::IFlag => { self.interrupt_disable = value; }
            Flags::DFlag => { self.decimal_mode_flag = value; }
            Flags::VFlag => { self.overflow_flag = value; }
            Flags::NFlag => { self.negative_flag = value; }
        }
    }

    pub fn get_flag_value(&self, flag: Flags) -> u8 {
        match flag {
            Flags::CFlag => self.carry_flag as u8,
            Flags::ZFlag => self.zero_flag as u8,
            Flags::IFlag => self.interrupt_disable as u8,
            Flags::DFlag => self.decimal_mode_flag as u8,
            Flags::VFlag => self.overflow_flag as u8,
            Flags::NFlag => self.negative_flag as u8,
        }
    }
}