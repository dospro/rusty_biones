mod rom_loader;
mod memory_map;
mod cpu;
mod mmc;
mod ppu;
mod control_pad;

use sdl2::pixels::PixelFormatEnum;
use sdl2::event::Event;
use memory_map::MemoryMap;
use control_pad::NESButtons;
use sdl2::keyboard::Scancode;
use std::collections::HashSet;
use crate::ppu::{ScreenBuffer, BUFFER_WIDTH, BUFFER_HEIGHT};


const SCREEN_WIDTH: u32 = BUFFER_WIDTH as u32 * 3;
const SCREEN_HEIGHT: u32 = BUFFER_HEIGHT as u32 * 3;


pub fn run() {
    let sdl_context = sdl2::init().unwrap();
    let video_system = sdl_context.video().unwrap();

    let window = video_system.window(
        "BioNes v1.0",
        SCREEN_WIDTH,
        SCREEN_HEIGHT)
        .position_centered()
        .opengl()
        .build()
        .unwrap();

    let mut canvas = window.into_canvas().build().unwrap();
    let texture_creator = canvas.texture_creator();
    let mut texture = texture_creator
        .create_texture_streaming(
            PixelFormatEnum::ARGB8888,
            BUFFER_WIDTH as u32,
            BUFFER_HEIGHT as u32)
        .unwrap();

    let mut event_pump = sdl_context.event_pump().unwrap();
    let mut is_running = true;

    let mut emulator = cpu::Cpu::new("roms/snow.nes");

    while is_running == true {
        for event in event_pump.poll_iter() {
            match event {
                Event::Quit { .. } => is_running = false,
                _ => {}
            }
        }
        let keys = HashSet::from(event_pump
            .keyboard_state()
            .pressed_scancodes()
            .map(|sdl_scancode| {
                match sdl_scancode {
                    Scancode::Z => NESButtons::A,
                    Scancode::X => NESButtons::B,
                    Scancode::Return => NESButtons::Start,
                    Scancode::RShift => NESButtons::Select,
                    Scancode::Up => NESButtons::Up,
                    Scancode::Down => NESButtons::Down,
                    Scancode::Left => NESButtons::Left,
                    Scancode::Right => NESButtons::Right,
                    Scancode::Escape => {
                        is_running = false;
                        NESButtons::None
                    }
                    _ => NESButtons::None,
                }
            })
            .collect()
        );
        emulator.update_input(keys);
        emulator.run_frame();
        let display_buffer = emulator.get_framebuffer();

        texture.with_lock(None, |buffer: &mut [u8], pitch: usize| {
            for y in 0..BUFFER_HEIGHT {
                for x in 0..BUFFER_WIDTH {
                    let offset = y * pitch + x * 4;
                    for b in 0..4 {
                        buffer[offset + b] = ((display_buffer[y * BUFFER_WIDTH + x] >> (8 * b)) & 0xFF) as u8;
                    }
                }
            }
        });

        canvas.copy(&texture, None, None);
        canvas.present();
    }
}
