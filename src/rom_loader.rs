use std::io::prelude::*;
use std::io::SeekFrom;
use std::rc::Rc;
use std::cell::RefCell;
use super::mmc::{MapperInterface, MirrorType};
use super::mmc::nrom::NROM;


pub struct Loader {
    filename: String,
    rom: Vec<Vec<u8>>,
    vrom: Vec<Vec<u8>>,
    mapper_id: u8,
    mirror_type: MirrorType,
    has_trainer: bool,
    has_save_ram: bool,
}

impl Loader {
    pub fn load_rom(filename: &str) -> Result<Loader, std::io::Error> {
        println!("Loading rom {}", filename);
        let mut file = std::fs::File::open(filename)?;
        let mut header: [u8; 0x10] = [0; 0x10];
        file.read(&mut header[..])?;

        let rom_banks = header[0x4];
        let chr_rom_banks = header[0x5];
        let has_save_ram = (header[0x6] >> 1) & 1 == 1;
        let has_trainer = (header[0x6] >> 2) & 1 == 1;
        let mirror_type = if ((header[0x6] >> 3) & 1) == 1 {
            MirrorType::FourScreen
        } else if (header[0x6] & 1) == 1 {
            MirrorType::VerticalMirror
        } else {
            MirrorType::HorizontalMirror
        };
        let mapper_id = (header[0x7] & 0xF0) | (header[0x6] >> 4);

        if has_trainer {
            // Read trainer into memory
            file.seek(SeekFrom::Current(0x200))?;
        }

        let mut rom = Vec::new();
        for _ in 0..rom_banks {
            let mut bank_data = Vec::with_capacity(0x4000);
            Read::by_ref(&mut file).take(0x4000).read_to_end(&mut bank_data)?;
            rom.push(bank_data);
        }

        let mut vrom = Vec::new();
        for _ in 0..chr_rom_banks {
            let mut bank_data = Vec::with_capacity(0x2000);
            Read::by_ref(&mut file).take(0x2000).read_to_end(&mut bank_data)?;
            vrom.push(bank_data);
        }

        Ok(Loader {
            filename: String::from(filename),
            rom,
            vrom,
            mapper_id,
            mirror_type,
            has_trainer,
            has_save_ram,
        })
    }

    pub fn get_mmc_interface(&mut self) -> Result<Rc<RefCell<dyn MapperInterface>>, String> {
        let mapper = match self.mapper_id {
            0 => {
                NROM::new(
                    std::mem::replace(&mut self.rom, Vec::new()),
                    std::mem::replace(&mut self.vrom, Vec::new()),
                    self.mirror_type,
                )
            }
            _ => {
                return Err("Unsupported mapper".to_string());
            }
        };
        Ok(Rc::new(RefCell::new(mapper)))
    }
}
