use std::rc::Rc;
use std::cell::RefCell;
use std::collections::HashSet;

use super::rom_loader;
use super::ppu::{Ppu, ScreenBuffer};
use super::control_pad::ControlPad;
use super::mmc::MapperInterface;
use super::control_pad::NESButtons;


pub enum InterruptType {
    Reset,
    NMI,
    Break,
    IRQ,
    None,
}

pub struct MemoryMap {
    mmc: Rc<RefCell<dyn MapperInterface>>,
    ram: Vec<u8>,
    ppu: Ppu,
    pad1: ControlPad,
}

impl MemoryMap {
    pub fn new(filename: &str) -> Result<MemoryMap, String> {
        println!("Creating rom loader for rom {}", filename);
        let mut loaded_rom = rom_loader::Loader::load_rom(filename).map_err(|e| e.to_string())?;
        let result = loaded_rom.get_mmc_interface()?;
        Ok(MemoryMap {
            mmc: result.clone(),
            ppu: Ppu::new(result.clone()),
            ram: vec![0; 0x800],
            pad1: ControlPad::new(),
        })
    }

    pub fn read_byte(&mut self, address: u16) -> u8 {
        if address < 0x2000 {
            return self.ram[(address as usize) & 0x7FF];
        } else if address < 0x4000 {
            return self.ppu.read(address);
        } else if address < 0x4020 {
            match address {
                0x4016 | 0x4017 => self.pad1.read(address),
                _ => 0
            }            // apu.read_register(address&0xF);
        } else {
            return self.mmc.borrow_mut().cpu_read_request(address);
        }
    }

    pub fn read_word(&mut self, address: u16) -> u16 {
        let low_byte = self.read_byte(address) as u16;
        let high_byte = self.read_byte(address + 1) as u16;
        (high_byte << 8) | low_byte
    }

    pub fn write_byte(&mut self, address: u16, value: u8) {
        if address < 0x2000 {
            self.ram[(address & 0x7FF) as usize] = value;
        } else if address < 0x4000 {
            self.ppu.write(address, value);
        } else if address < 0x4014 {
            // apu registers
        } else if address == 0x4014 {
            self.dma_transfer(value);
        } else if address < 0x4020 {
            match address {
                0x4016 | 0x4017 => self.pad1.write(address, value),
                _ => {}
            }
            // io registers
        } else {
            self.mmc.borrow_mut().cpu_write_request(address, value);
        }
    }

    /// Checks ppu, apu and memory mappers and returns the type of
    /// Interrupt that needs handling by the cpu
    pub fn get_next_interrupt(&mut self) -> InterruptType {
        if self.ppu.needs_nmi_handling() {
            return InterruptType::NMI;
        }
        return InterruptType::None;
    }

    pub fn update(&mut self, cpu_cycles: u32) {
        self.ppu.update(cpu_cycles);
    }

    pub fn update_input(&mut self, input: HashSet<NESButtons>) {
        self.pad1.update_input(input);
    }

    pub fn get_framebuffer(&self) -> &ScreenBuffer {
        return self.ppu.get_framebuffer();
    }

    fn dma_transfer(&mut self, value: u8) {
        let address = (value as u16) << 8;
        for i in 0..0x100 {
            let data = self.read_byte(address + i);
            self.ppu.write(0x2004, data);
        }
    }
}