use std::collections::HashSet;

#[derive(Eq, PartialEq, Hash)]
pub enum NESButtons {
    A,
    B,
    Start,
    Select,
    Up,
    Down,
    Left,
    Right,
    None,
}

enum DeviceMode {
    PollMode,
    ReadMode,
}

pub struct ControlPad {
    mode: DeviceMode,
    shift_register: u8,
    pressed_keys: HashSet<NESButtons>,
}

impl ControlPad {
    pub fn new() -> Self {
        ControlPad {
            mode: DeviceMode::ReadMode,
            shift_register: 0,
            pressed_keys: HashSet::new(),
        }
    }
    pub fn read(&mut self, address: u16) -> u8 {
        match address {
            0x4016 => {
                let result = self.shift_register & 1;
                self.shift_register /= 2;
                return result;
            }
            0x4017 => { return 0; }
            _ => { return 0; }
        };
    }

    pub fn write(&mut self, address: u16, value: u8) {
        match address {
            0x4016 => {
                if (value & 1) == 0 {
                    self.mode = DeviceMode::PollMode;
                } else {
                    self.mode = DeviceMode::ReadMode;
                    self.poll_controller();
                }
            }
            0x4017 => {}
            _ => {}
        }
    }

    pub fn update_input(&mut self, input: HashSet<NESButtons>) {
        self.pressed_keys = input;
    }

    fn poll_controller(&mut self) {
        self.shift_register = 0;
        if self.pressed_keys.contains(&NESButtons::A) {
            self.shift_register |= 1;
        }
        if self.pressed_keys.contains(&NESButtons::B) {
            self.shift_register |= 2;
        }
        if self.pressed_keys.contains(&NESButtons::Select) {
            self.shift_register |= 4;
        }
        if self.pressed_keys.contains(&NESButtons::Start) {
            self.shift_register |= 8;
        }
        if self.pressed_keys.contains(&NESButtons::Up) {
            self.shift_register |= 0x10;
        }
        if self.pressed_keys.contains(&NESButtons::Down) {
            self.shift_register |= 0x20;
        }
        if self.pressed_keys.contains(&NESButtons::Left) {
            self.shift_register |= 0x40;
        }
        if self.pressed_keys.contains(&NESButtons::Right) {
            self.shift_register |= 0x80;
        }
    }
}