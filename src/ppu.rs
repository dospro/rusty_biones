use std::rc::Rc;
use std::cell::RefCell;

use super::mmc::{MapperInterface, MirrorType};

pub const BUFFER_WIDTH: usize = 256;
pub const BUFFER_HEIGHT: usize = 240;
pub const DISPLAY_BUFFER_SIZE: usize = BUFFER_HEIGHT * BUFFER_WIDTH;

pub type ScreenBuffer = [u32; DISPLAY_BUFFER_SIZE];

pub struct Ppu {
    mmc: Rc<RefCell<dyn MapperInterface>>,
    ciram: [u8; 0x800],
    oam: [u8; 0x100],
    palettes: [u8; 0x20],
    colors: [u32; 0x40],
    ppu_ctrl: PpuControlRegister,
    ppu_mask: PpuMaskRegister,
    ppu_status: PpuStatusRegister,
    oam_address: u8,
    flip_flop: bool,
    scroll_x_offset: u8,
    scroll_y_offset: u8,
    vram_address: u16,
    vram_value_latch: u8,
    display_buffer: ScreenBuffer,
    ppu_cycles_counter: u32,
    current_scanline: u32,
    nmi_signal: bool,

}

impl Ppu {
    pub fn new(mmc_interface: Rc<RefCell<dyn MapperInterface>>) -> Self {
        let mut palette: [u32; 0x40] = [0; 0x40];
        // @formatter:off
        palette[0]=0x757575;	palette[16]=0xBCBCBC;	palette[32]=0xFFFFFF;	palette[48]=0xFFFFFF;
        palette[1]=0x271B8F;	palette[17]=0x0093EF;	palette[33]=0x3FBFFF;	palette[49]=0xABE7FF;
        palette[2]=0x0000AB;	palette[18]=0x233BEF;	palette[34]=0x5F97FF;	palette[50]=0xC7D7FF;
        palette[3]=0x47009F;	palette[19]=0x8300F3;	palette[35]=0xA78BFD;	palette[51]=0xD7CBFF;
        palette[4]=0x8F0077;	palette[20]=0xBF00BF;	palette[36]=0xF77BFF;	palette[52]=0xFFC7FF;
        palette[5]=0xAB0013;	palette[21]=0xE7005B;	palette[37]=0xFF77B7;	palette[53]=0xFFC7DB;
        palette[6]=0xA70000;	palette[22]=0xDB2B00;	palette[38]=0xFF7763;	palette[54]=0xFFBFB3;
        palette[7]=0x7F0B00;	palette[23]=0xCB4F0F;	palette[39]=0xFF9B3B;	palette[55]=0xFFDBAB;
        palette[8]=0x432F00;	palette[24]=0x8B7300;	palette[40]=0xF3BF3F;	palette[56]=0xFFE7A3;
        palette[9]=0x004700;	palette[25]=0x009700;	palette[41]=0x83D313;	palette[57]=0xE3FFA3;
        palette[10]=0x005100;	palette[26]=0x00AB00;	palette[42]=0x4FDF4B;	palette[58]=0xABF3BF;
        palette[11]=0x003F17;	palette[27]=0x00933B;	palette[43]=0x58F898;	palette[59]=0xB3FFCF;
        palette[12]=0x1B3F5F;	palette[28]=0x00838B;	palette[44]=0x00EBDB;	palette[60]=0x9FFFF3;
        palette[13]=0x000000;	palette[29]=0x000000;	palette[45]=0x000000;	palette[61]=0x000000;
        palette[14]=0x000000;	palette[30]=0x000000;	palette[46]=0x000000;	palette[62]=0x000000;
        palette[15]=0x000000;	palette[31]=0x000000;	palette[47]=0x000000;	palette[63]=0x000000;
        // @formatter:on

        Ppu {
            mmc: mmc_interface,
            ciram: [0; 0x800],
            oam: [0; 0x100],
            palettes: [0; 0x20],
            colors: palette,
            ppu_ctrl: Default::default(),
            ppu_mask: Default::default(),
            ppu_status: Default::default(),
            oam_address: 0,
            flip_flop: false,
            scroll_x_offset: 0,
            scroll_y_offset: 0,
            vram_address: 0,
            vram_value_latch: 0,
            display_buffer: [0; DISPLAY_BUFFER_SIZE],
            ppu_cycles_counter: 0,
            current_scanline: 0,
            nmi_signal: false,
        }
    }

    pub fn read(&mut self, address: u16) -> u8 {
        match address {
            0x2000 => return 0xFF,
            0x2001 => return 0xFF,
            0x2002 => {
                self.flip_flop = false;
                u8::from(&mut self.ppu_status)
            }
            0x2003 => return 0xFF,
            0x2004 => return self.oam[self.oam_address as usize],
            0x2005 => return 0xFF,
            0x2006 => return 0xFF,
            0x2007 => {
                let result = self.vram_value_latch;
                self.vram_value_latch = self.internal_read(self.vram_address);
                if self.vram_address >= 0x3F00 {
                    self.vram_address = self.vram_address.wrapping_add(self.ppu_ctrl.increment_steps as u16) & 0x3FFF;
                    return self.vram_value_latch;
                }
                self.vram_address = self.vram_address.wrapping_add(self.ppu_ctrl.increment_steps as u16) & 0x3FFF;
                return result;
            }
            _ => return 0xFF
        }
    }

    pub fn write(&mut self, address: u16, value: u8) {
        match address {
            0x2000 => {
                self.ppu_ctrl = PpuControlRegister::from(value);
            }
            0x2001 => {
                self.ppu_mask = PpuMaskRegister::from(value);
            }
            0x2002 => {}
            0x2003 => {
                self.oam_address = value;
            }
            0x2004 => {
                self.oam[self.oam_address as usize] = value;
                self.oam_address = self.oam_address.wrapping_add(1);
            }
            0x2005 => {
                if self.flip_flop {
                    self.scroll_y_offset = value;
                } else {
                    self.scroll_x_offset = value;
                }
                self.flip_flop = !self.flip_flop;
            }
            0x2006 => {
                if !self.flip_flop {
                    self.vram_address &= 0b0000_0000_1111_1111;
                    self.vram_address |= (value as u16) << 8;
                } else {
                    self.vram_address &= 0b1111_1111_0000_0000;
                    self.vram_address |= value as u16;
                }
                self.flip_flop = !self.flip_flop;
            }
            0x2007 => {
                self.internal_write(self.vram_address, value);
                self.vram_address = self.vram_address.wrapping_add(self.ppu_ctrl.increment_steps as u16);
            }
            _ => panic!("Forbidden address in Ppu")
        }
    }


    pub fn needs_nmi_handling(&mut self) -> bool {
        // use a latch?
        if self.ppu_ctrl.nim_on_vblank && self.ppu_status.is_in_vblank && !self.nmi_signal {
            self.nmi_signal = true;
            return true;
        }
        return false;
    }

    pub fn update(&mut self, cpu_cycles: u32) {
        // One cpu cycle equals to 3 ppu cycles.
        let ppu_cycles = cpu_cycles * 3;
        self.ppu_cycles_counter += ppu_cycles;
        let scanline = self.ppu_cycles_counter / 341;
        if scanline == self.current_scanline {
            // If last scanline hasn't finished, we do nothing
            return;
        }

        self.current_scanline = scanline;
        if scanline > 0 && scanline < 241 {
            // Then, for each scanline (240 scanlines), we update video_buffer to draw that scanline
            // We must be careful. Each scanline should be render after the cycle count has finished.
            self.draw_scanline(scanline - 1);
        } else if scanline > 241 && scanline < 262 && !self.ppu_status.is_in_vblank {
            // Finally we set v-blank flag for 20 scanlines
            self.ppu_status.is_in_vblank = true;
        } else if scanline >= 262 {
            // After that period we reset the v-blank flag and start over
            let cycles_per_frame = 341 * 262;
            self.ppu_cycles_counter -= cycles_per_frame;
            self.ppu_status.is_in_vblank = false;
            self.nmi_signal = false;
            self.ppu_status.sprite_overflow = false;
            self.ppu_status.sprite_hit = false;
        }
    }

    pub fn get_framebuffer(&self) -> &ScreenBuffer {
        return &self.display_buffer;
    }

    fn internal_read(&self, address: u16) -> u8 {
        if address < 0x2000 {
            return self.mmc.borrow_mut().ppu_read_request(address);
        } else if address < 0x2400 {
            return self.ciram[(address as usize) & 0x3FF];
        } else if address < 0x2800 {
            match self.mmc.borrow().get_mirror_type() {
                MirrorType::HorizontalMirror | MirrorType::SingleScreen => {
                    return self.ciram[(address as usize) & 0x3FF];
                }
                _ => return self.ciram[(address as usize) & 0x7FF]
            }
        } else if address < 0x2C00 {
            match self.mmc.borrow().get_mirror_type() {
                MirrorType::VerticalMirror | MirrorType::SingleScreen => {
                    return self.ciram[(address as usize) & 0x3FF];
                }
                _ => return self.ciram[0x400 | ((address as usize) & 0x3FF)]
            }
        } else if address < 0x3000 {
            match self.mmc.borrow().get_mirror_type() {
                MirrorType::SingleScreen => return self.ciram[(address as usize) & 0x3FF],
                _ => return self.ciram[0x400 | ((address as usize) & 0x3FF)]
            }
        } else if address < 0x3F00 {
            return self.internal_read(address - 0x1000);
        } else {
            let palette_address = match self.vram_address {
                0x3F10 | 0x3F14 | 0x3F18 | 0x3F1C => self.vram_address & 0b1110_1111,
                _ => self.vram_address
            };
            return self.palettes[(palette_address as usize) & 0x1F];
        }
    }

    fn internal_write(&mut self, address: u16, value: u8) {
        if address < 0x2000 {
            self.mmc.borrow_mut().ppu_write_request(address, value);
        } else if address < 0x2400 {
            self.ciram[(address as usize) & 0x3FF] = value;
        } else if address < 0x2800 {
            match self.mmc.borrow().get_mirror_type() {
                MirrorType::HorizontalMirror | MirrorType::SingleScreen => {
                    self.ciram[(address as usize) & 0x3FF] = value;
                }
                _ => self.ciram[(address as usize) & 0x7FF] = value
            }
        } else if address < 0x2C00 {
            match self.mmc.borrow().get_mirror_type() {
                MirrorType::VerticalMirror | MirrorType::SingleScreen => {
                    self.ciram[(address as usize) & 0x3FF] = value;
                }
                _ => self.ciram[0x400 | ((address as usize) & 0x3FF)] = value
            }
        } else if address < 0x3000 {
            match self.mmc.borrow().get_mirror_type() {
                MirrorType::SingleScreen => self.ciram[(address as usize) & 0x3FF] = value,
                _ => self.ciram[0x400 | ((address as usize) & 0x3FF)] = value
            }
        } else if address < 0x3F00 {
            self.internal_write(address - 0x1000, value);
        } else {
            let palette_address = match self.vram_address {
                0x3F10 | 0x3F14 | 0x3F18 | 0x3F1C => self.vram_address & 0b1110_1111,
                _ => self.vram_address
            };
            //println!("Address: {:X?} Value: {:X?}", palette_address, value);
            self.palettes[(palette_address as usize) & 0x1F] = value;
        }
    }

    fn draw_scanline(&mut self, scanline_number: u32) {
        if self.ppu_mask.show_background {
            self.draw_background_scanline(scanline_number);
        }
        if self.ppu_mask.show_sprites {
            self.draw_sprites_scanline(scanline_number);
        }
    }

    fn draw_background_scanline(&mut self, scanline_number: u32) {
        // Each scanlines has 32 tiles
        // Each tile is 8x8
        let y_offset = ((scanline_number as u16) / 8) * 32;

        // Each lines has 32 tiles
        for tile_in_line in 0..32 {
            // First we need to know what tiles to use. That info is in the nametable
            let tile_index = self.internal_read(self.ppu_ctrl.name_table + y_offset + tile_in_line);
            // Once we got the tile index, we can retrieve it from the pattern table
            // Each byte in the attribute table defines a palette number for a 32x32 area (4x4 tiles)
            // The attribute table is a matrix of 8x8
            //let attribute_table_offset = attribute_table_address + (((scanline_number as u16) / 32) * 8) + tile_in_line / 4;
            //let attribute = self.internal_read(attribute_table_offset);

//            let y_quadrant = ((scanline_number as u16) >> 4) & 1;
//            let x_quadrant = (tile_in_line >> 3) & 1;
//            let upper_bits = (y_quadrant << 1) | x_quadrant;
//            let palette_number = (attribute >> (upper_bits * 2)) & 0b11;

            // Now fetch the pattern
            let pattern_table_offset = self.ppu_ctrl.background_pattern_table + (tile_index as u16) * 16 + ((scanline_number as u16) & 7);
            let mut low_byte = self.internal_read(pattern_table_offset);
            let mut high_byte = self.internal_read(pattern_table_offset + 8);

            // Send pattern to video_buffer

            for x in (0..8).rev() {
                let x_position = tile_in_line * 8 + x;
                if x_position < 256 {
                    let palette_index = self.palettes[(((high_byte as usize) & 1) << 1) | ((low_byte as usize) & 1)];
                    let offset = scanline_number as usize * BUFFER_WIDTH + x_position as usize;
                    self.display_buffer[offset] = self.colors[palette_index as usize];
                }
                high_byte >>= 1;
                low_byte >>= 1;
            }
        }
    }

    fn draw_sprites_scanline(&mut self, scanline_number: u32) {
        // Get the first 8 sprites that are on the line
        let secondary_oam: Vec<&[u8]> = self.oam.chunks(4)
            .filter(|sprite| {
                let y_position = sprite[0] as u32 + 1;
                y_position <= scanline_number && (y_position + self.ppu_ctrl.sprite_size as u32) >= scanline_number
            }).collect();
        if secondary_oam.len() > 8 {
            self.ppu_status.sprite_overflow = true;
        }

        for sprite in secondary_oam.iter().rev() {
            let is_sprite_0 = sprite == secondary_oam.iter().last().unwrap();
            let y_position = (sprite[0] as u16) + 1;
            let horizontal_mirror = (sprite[2] & 0b0100_0000) != 0;
            let vertical_mirror = (sprite[2] & 0b1000_0000) != 0;
            let palette = ((sprite[2] & 0b11) << 2) as usize;
            let priority = (sprite[2] & 0b0010_0000) != 0;

            let pattern_table = if self.ppu_ctrl.sprite_size == 7 {
                self.ppu_ctrl.sprites_pattern_table
            } else {
                0x1000 * (sprite[1] as u16 & 1)
            };

            let y_offset = scanline_number as u16 - y_position;

            let sprite_pattern_offset = if vertical_mirror {
                pattern_table +
                    self.ppu_ctrl.sprite_size as u16 -
                    y_offset & (self.ppu_ctrl.sprite_size as u16) +
                    (sprite[1] as u16) * 16
            } else {
                pattern_table | ((sprite[1] as u16) * 16) | (y_offset & 7)
            };

            let mut low_byte = self.internal_read(sprite_pattern_offset);
            let mut high_byte = self.internal_read(sprite_pattern_offset + 8);

            let backdrop_color = self.colors[self.palettes[0] as usize];

            for x in (0..8).rev() {
                let x_position = if horizontal_mirror { sprite[3] as usize + 7 - x } else { (sprite[3] as usize) + x };
                let palette_index = palette | (((high_byte as usize) & 1) << 1) | ((low_byte as usize) & 1);
                let palette_color = self.palettes[0x10 | palette_index] as usize;
                if x_position < 256 && palette_index != 0 && palette_index != 4 && palette_index != 8 && palette_index != 0xC {
                    let offset = scanline_number as usize * BUFFER_WIDTH + x_position;
                    if is_sprite_0 {
                        if self.display_buffer[offset] != backdrop_color && self.colors[palette_index] != backdrop_color {
                            self.ppu_status.sprite_hit = true;
                        }
                    }
                    self.display_buffer[offset] = self.colors[palette_color];
                }
                low_byte >>= 1;
                high_byte >>= 1;
            }
        }
    }
}

#[derive(Default)]
struct PpuControlRegister {
    name_table: u16,
    increment_steps: u8,
    sprites_pattern_table: u16,
    background_pattern_table: u16,
    sprite_size: u8,
    nim_on_vblank: bool,
}

impl From<u8> for PpuControlRegister {
    fn from(value: u8) -> Self {
        PpuControlRegister {
            name_table: 0x2000 | ((value as u16) & 0b0011) << 10,
            increment_steps: if (value & 0b0100) != 0 { 32 } else { 1 },
            sprites_pattern_table: ((value as u16) & 0b1000) << 9,
            background_pattern_table: ((value as u16) & 0b0001_0000) << 8,
            sprite_size: if (value & 0b0010_0000) != 0 { 15 } else { 7 },
            nim_on_vblank: (value >> 7) != 0,
        }
    }
}

#[derive(Default)]
struct PpuMaskRegister {
    is_color: bool,
    clip_background: bool,
    clip_sprites: bool,
    show_background: bool,
    show_sprites: bool,
    color_emphasis: u8,
}

impl From<u8> for PpuMaskRegister {
    fn from(value: u8) -> Self {
        PpuMaskRegister {
            is_color: (value & 0b0001) != 0,
            clip_background: (value & 0b0010) == 0,
            clip_sprites: (value & 0b0100) == 0,
            show_background: (value & 0b1000) != 0,
            show_sprites: (value & 0b0001_0000) != 0,
            color_emphasis: (value >> 5),
        }
    }
}

#[derive(Default)]
struct PpuStatusRegister {
    sprite_overflow: bool,
    sprite_hit: bool,
    is_in_vblank: bool,
}

impl From<&mut PpuStatusRegister> for u8 {
    fn from(value: &mut PpuStatusRegister) -> Self {
        let v_blank = value.is_in_vblank as u8;
        value.is_in_vblank = false;
        ((v_blank as u8) << 7) | ((value.sprite_hit as u8) << 6) | ((value.sprite_overflow as u8) << 5)
    }
}